---
title: "Source code (JS)"
draft: false
TableOfContents: true
---

## Modules

<table>
  <thead>
    <tr>
      <th>Module</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td><a href="#module_actions/add-document-page">actions/add-document-page</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_actions/add-file">actions/add-file</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_actions/generate-doc">actions/generate-doc</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_functions/add-license-snippet">functions/add-license-snippet</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_functions/date-today">functions/date-today</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_functions/doc-path-creator">functions/doc-path-creator</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_functions/get-directory-and-sub-directory">functions/get-directory-and-sub-directory</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_prompts/add-document-page">prompts/add-document-page</a></td>
    <td></td>
    </tr>
<tr>
    <td><a href="#module_prompts/add-file">prompts/add-file</a></td>
    <td></td>
    </tr>
</tbody>
</table>

<a name="module_actions/add-document-page"></a>

## actions/add-document-page
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_actions/add-file"></a>

## actions/add-file
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_actions/generate-doc"></a>

## actions/generate-doc
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_actions/generate-doc..generateDocs"></a>

### actions/generate-doc~generateDocs()
This generates the doc files from source code.

**Kind**: inner method of [<code>actions/generate-doc</code>](#module_actions/generate-doc)

* * *

<a name="module_functions/add-license-snippet"></a>

## functions/add-license-snippet
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* [functions/add-license-snippet](#module_functions/add-license-snippet)
    * [~hashLicense()](#module_functions/add-license-snippet..hashLicense) ⇒ <code>string</code>
    * [~commentLicenseBlock()](#module_functions/add-license-snippet..commentLicenseBlock) ⇒ <code>string</code>
    * [~jsCommentLicense(description)](#module_functions/add-license-snippet..jsCommentLicense) ⇒ <code>string</code>
    * [~tripleSlashLicense()](#module_functions/add-license-snippet..tripleSlashLicense) ⇒ <code>string</code>
    * [~pythonLicense()](#module_functions/add-license-snippet..pythonLicense) ⇒ <code>string</code>
    * [~addLicenseSnippet()](#module_functions/add-license-snippet..addLicenseSnippet) ⇒ <code>string</code>


* * *

<a name="module_functions/add-license-snippet..hashLicense"></a>

### functions/add-license-snippet~hashLicense() ⇒ <code>string</code>
creates a license snippet for Hash comment type of files

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the license snippet

* * *

<a name="module_functions/add-license-snippet..commentLicenseBlock"></a>

### functions/add-license-snippet~commentLicenseBlock() ⇒ <code>string</code>
creates a license snippet for Comment block type of files

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the license snippet

* * *

<a name="module_functions/add-license-snippet..jsCommentLicense"></a>

### functions/add-license-snippet~jsCommentLicense(description) ⇒ <code>string</code>
creates a license snippet for JS files

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the license snippet

| Param | Type |
| --- | --- |
| description | <code>string</code> |


* * *

<a name="module_functions/add-license-snippet..tripleSlashLicense"></a>

### functions/add-license-snippet~tripleSlashLicense() ⇒ <code>string</code>
creates a license snippet for Triple slash type of files

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the license snippet

* * *

<a name="module_functions/add-license-snippet..pythonLicense"></a>

### functions/add-license-snippet~pythonLicense() ⇒ <code>string</code>
creates a license snippet for Python type of files

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the license snippet

* * *

<a name="module_functions/add-license-snippet..addLicenseSnippet"></a>

### functions/add-license-snippet~addLicenseSnippet() ⇒ <code>string</code>
Creates a snippet of license given a file

**Kind**: inner method of [<code>functions/add-license-snippet</code>](#module_functions/add-license-snippet)
**Returns**: <code>string</code> - the snippet
**this**: <code>AddFilePrompt</code>

* * *

<a name="module_functions/date-today"></a>

## functions/date-today
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="exp_module_functions/date-today--module.exports"></a>

### module.exports() ⇒ <code>string</code> ⏏
This is for the doc creation

**Kind**: Exported function
**Returns**: <code>string</code> - date today in ISO String

* * *

<a name="module_functions/doc-path-creator"></a>

## functions/doc-path-creator
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_functions/doc-path-creator..docPathCreator"></a>

### functions/doc-path-creator~docPathCreator() ⇒ <code>String</code>
This creates a path for the documentation that will
be added depending if it is a pageIndex or it is in root

**Kind**: inner method of [<code>functions/doc-path-creator</code>](#module_functions/doc-path-creator)
**this**: <code>AddDocumentPagePrompt</code>

* * *

<a name="module_functions/get-directory-and-sub-directory"></a>

## functions/get-directory-and-sub-directory
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_functions/get-directory-and-sub-directory..getDirectoryAndSubdirectory"></a>

### functions/get-directory-and-sub-directory~getDirectoryAndSubdirectory(currPath) ⇒ <code>Array.&lt;string&gt;</code>
Creates a doc file given a path

**Kind**: inner method of [<code>functions/get-directory-and-sub-directory</code>](#module_functions/get-directory-and-sub-directory)

| Param | Type |
| --- | --- |
| currPath | <code>string</code> |


* * *

<a name="module_prompts/add-document-page"></a>

## prompts/add-document-page
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *

<a name="module_prompts/add-file"></a>

## prompts/add-file
**License**: Copyright 2020, UPLB HCI Lab Group.
Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

* * *
