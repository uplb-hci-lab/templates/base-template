const { capitalCase } = require('change-case');
const docPathCreator = require('./plop-utils/functions/doc-path-creator');
const dateToday = require('./plop-utils/functions/date-today');
const addLicenseSnippet = require('./plop-utils/functions/add-license-snippet');

module.exports = plop => {
  // general helpers
  plop.setHelper('docPathCreator', docPathCreator);
  plop.setHelper('capitalCase', capitalCase);
  plop.setHelper('dateToday', dateToday);
  plop.setHelper('addLicenseSnippet', addLicenseSnippet);

  // generator setting
  plop.setGenerator('create:file', {
    description: 'Create a new file',
    prompts: require('./plop-utils/prompts/add-file'),
    actions: require('./plop-utils/actions/add-file')
  });

  plop.setGenerator('create:doc', {
    description: 'Add a document page',
    prompts: require('./plop-utils/prompts/add-document-page'),
    actions: require('./plop-utils/actions/add-document-page')
  });

  plop.setGenerator('generate:doc', {
    description: 'Generate code documentation from source code',
    prompts: [],
    actions: require('./plop-utils/actions/generate-doc')
  });
};
