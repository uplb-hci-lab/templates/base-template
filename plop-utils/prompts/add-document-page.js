/**
 * @module
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const getDirectoryAndSubdirectory = require('../functions/get-directory-and-sub-directory');

module.exports = [
  {
    type: 'input',
    name: 'pageTitle',
    message: 'Page Title',
    /**
     * @param {string} value
     * @ignore
     */
    validate: (value) => {
      if ((/.+/).test(value) && value !== null) { return true; }
      return 'Page Title is Required';
    }
  },
  {
    type: 'list',
    name: 'path',
    message: 'What would be the path (if it is a subchapter) of the page relative to the docs folder (leave blank if it is a main chapter of doc)',
    choices: () => {
      const choices = getDirectoryAndSubdirectory('docs');
      choices.push('(Root)');
      return choices.map(item => item.replace('docs/', ''));
    },
    /**
     * @param {string} value
     * @return {string | null}
     * @ignore
     */
    filter: value => {
      return value === '(Root)' ? null : value;
    }
  },
  {
    type: 'confirm',
    name: 'pageIndex',
    message: 'Will this be a folder index?',
    default: true,
    /**
     * @param {{ path: string }} data
     * @return {string}
     * @ignore
     */
    when: data => {
      const { path } = data;
      return path;
    }
  },
  {
    type: 'input',
    name: 'weight',
    default: 0,
    message: 'What would be the order of this page related to others? (will be multiplied to 10 after)',
    /**
     * @param {number} value
     * @return {string | boolean}
     * @ignore
     */
    validate: value => {
      if (isNaN(value)) {
        return 'Please put an integer';
      }
      return true;
    },
    /**
     * @param {string} value
     * @return {number}
     * @ignore
     */
    filter: value => parseInt(value) * 10
  }
];
