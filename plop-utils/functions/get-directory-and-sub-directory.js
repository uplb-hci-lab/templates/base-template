/**
 * @module
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const { readdirSync, lstatSync } = require('fs');

/**
 * @function
 * @description Creates a doc file given a path
 * @param {string} currPath
 * @returns {Array<string>}
 */
const getDirectoryAndSubdirectory = currPath => {
  const choices = [];
  const dirs = readdirSync(currPath);
  for (const dir of dirs) {
    const newPath = `${currPath}/${dir}`;
    if (lstatSync(newPath).isDirectory()) {
      choices.push(newPath);
      const newChoices = getDirectoryAndSubdirectory(newPath);
      for (const newDir of newChoices) {
        choices.push(newDir);
      }
    }
  }
  return choices;
};

module.exports = getDirectoryAndSubdirectory;
