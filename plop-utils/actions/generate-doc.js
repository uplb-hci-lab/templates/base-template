/**
 * @module
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const jsdoc2md = require('jsdoc-to-markdown');
const { writeFileSync } = require('fs');

const header = `---
title: "Source code (JS)"
draft: false
TableOfContents: true
weight: 10000000000
---
`;

/**
 * @function
 * @description This generates the doc files from source code.
 */
async function generateDocs () {
  const text = `${header}\n\n${await jsdoc2md.render({
    files: ['plop-utils/**/*.js'],
    // @ts-ignore
    separators: true,
    'module-index-format': 'table',
    'global-index-format': 'table'
  })}`;
  writeFileSync('docs/code/code-js.md', text, 'utf8');
}

module.exports = [
  generateDocs
];
