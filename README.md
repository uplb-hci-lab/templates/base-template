# Base Template [![pipeline status](https://gitlab.com/uplb-hci-lab/templates/base-template/badges/master/pipeline.svg)](https://gitlab.com/uplb-hci-lab/templates/base-template/-/commits/master)[![coverage report](https://gitlab.com/uplb-hci-lab/templates/base-template/badges/master/coverage.svg)](https://gitlab.com/uplb-hci-lab/templates/base-template/-/commits/master)

This is the base template for all of our projects.

Page Documentation can be found at: https://uplb-hci-lab.gitlab.io/templates/base-template/

# Installation

You should have the following:
- Node version 10 or 12

To install run,
```bash
npm install
```

# Usage

To run this project

```bash
npm start
```

# Documentation

Please visit the repo page or if you want to build your own copy here, you have to install Hugo globally

```bash
brew install hugo
```

If the command to install wasn't done while installing this repo, run this...

```bash
npm run install-docs
```

Then run the following commands:

```bash
npm run serve-docs
```

If you want to just build the docs,

```bash
npm run build-docs
```
